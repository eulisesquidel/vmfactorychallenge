USE [VMFactory]
GO


drop table [dbo].[WeatherHistory]

/****** Object:  Table [dbo].[TaskList]    Script Date: 17/12/2021 11:23:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[WeatherHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Country] [nvarchar](max) NULL,
	[City] [nvarchar](max) NULL,
	[Temperature] decimal(6,2) ,
	[FeelsLike]   decimal(6,2) ,
	[CreatedOn] [datetime] NULL,
	[UserID] [nvarchar](100) NULL
 CONSTRAINT [PK_WeatherHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO


INSERT INTO [WeatherHistory] ( 	[Country]  , [City] ,
						[Temperature] ,
						[FeelsLike] ,
						[CreatedOn],
						[UserID] ) 
VALUES ( 	'Argentina'  , 'Llavallol' , 30 , 31, getdate(), 'prueba')

set dateformat dmy

INSERT INTO [WeatherHistory] ([Country], [City], [Temperature], [FeelsLike], [CreatedOn], [UserID])
 VALUES('AR', 'Buenos Aires' , 29.69,-242.1,'20/12/2021 17:56:30','testing')

select * from [WeatherHistory] 


--truncate table [WeatherHistory]