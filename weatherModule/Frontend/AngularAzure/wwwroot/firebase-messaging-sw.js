importScripts('https://www.gstatic.com/firebasejs/7.15.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.15.0/firebase-messaging.js');

firebase.initializeApp({
    apiKey: "AIzaSyBmVSVni4osicVF0TLMcKRIy77c1PUMD70",
    authDomain: "todo-app-e430f.firebaseapp.com",
    databaseURL: "https://todo-app-e430f.firebaseio.com",
    projectId: "todo-app-e430f",
    storageBucket: "todo-app-e430f.appspot.com",
    messagingSenderId: "567468698903",
    appId: "1:567468698903:web:6de91efb185f4d199d1b52"
});

const messaging = firebase.messaging();

// Handle incoming messages. Called when:
// - a message is received while the app has focus
// - the user clicks on an app notification created by a service worker
//   `messaging.onBackgroundMessage` handler.
messaging.onMessage((payload) => {
    alert(payload);
    console.log('Message received. ', payload);
    // ...
  });