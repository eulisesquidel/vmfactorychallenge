export interface Weather {
    city:string,
    country:string,
    temperature:string,
    feelsLike:string
}
