
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { Weather } from 'src/app/models/weather';
import { DevService } from 'src/app/services/dev.service';
import { WeatherService } from 'src/app/services/weather.service';



const ELEMENT_DATA: Weather[] = [];


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {

  enProceso =false;
  weatherForm: FormGroup;
  createMode: boolean = true;
  ciudad:string;
  resulquery:Weather = { city:"",country:"",temperature:"",feelsLike:"" };
  incluirhistorico:boolean;
  displayedColumns: string[] = ['country', 'city', 'temperature', 'feelsLike'];
  //dataSource = ELEMENT_DATA;

  dataSource = new MatTableDataSource<Weather>([]);

  
  listadeciudades:  string[]=['Buenos Aires, AR','Córdoba, AR','Mendoza, AR','Lanús, AR' , 'Madrid, ES', 'París, FR','London, UK', 'Rome, IT']; 


  constructor(
    private formBuilder: FormBuilder,
    private devSVC : DevService,
    private weatherService:WeatherService
    ) { 
      
}

  ngOnInit() {


    this.weatherForm = this.formBuilder.group({
      id: ['', ''],
      ciudad:['', Validators.required],
      incluirhistorico:[false, Validators.required]
    }); // cierra formBuilder

 
      this.createMode =false;

  }

  onChangeCiudad(unaciudad:any){
    this.devSVC.debugger();
    console.log(unaciudad);
    this.resulquery = { city:"",country:"",temperature:"",feelsLike:"" };
    this.dataSource.data =[];
  }

  async submit(){



    if (this.ciudad==undefined){
      alert("Debe ingresar la Ciudad");
      return;
    }

    this.dataSource.data =[];
    this.enProceso=true;

    if (this.incluirhistorico){
      this.weatherService.weatherhistory(this.ciudad).then((resp:any[])=>{
        let lista : Weather[] =[];
        resp.forEach((item:Weather)=>{
          this.devSVC.debugger();
          const fila  : Weather = { country: item.country , city : item.city , temperature: item.temperature , feelsLike:item.feelsLike }
          lista.push(fila);
        
        })
        this.dataSource.data = lista;
        console.log (resp); 
      })
    }

    // get response from backend - a current weather by city
    this.resulquery = await this.weatherService.currentweather(this.ciudad);
    this.enProceso=false;

  }
}