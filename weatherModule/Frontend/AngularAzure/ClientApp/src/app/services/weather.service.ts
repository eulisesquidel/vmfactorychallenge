import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { DevService } from './dev.service';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private devSVC : DevService, private  http:HttpClient ) { }


  async currentweather(city_name:string){
    
    this.devSVC.debugger();
      
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let pathURL = environment.endpointweather + "/currentweather?cityname="+ city_name
    this.devSVC.log(pathURL);
    let resultado = await this.http.get<any>(pathURL, { headers: new HttpHeaders({
                                                                 "authorization": "lLmvbIpFvHRaI3nKOaSF2p9kFeVa/QhPKJDuYxie9JnPzl9w0YOgvA=="
                                             }) }).toPromise().then();
    this.devSVC.log(resultado);
    return resultado;
  }


  
  weatherhistory(city_name:string){
    
    this.devSVC.debugger();

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let pathURL = environment.endpointweather + "/weatherhistory?cityname="+ city_name
    this.devSVC.log(pathURL);
    let resultado =  this.http.get<any>(pathURL, { headers: new HttpHeaders({
                                                                 "authorization": "4tHHlsDVrHFvQx03fLhcaMY9MWWOQxM2hs1ETIa/MuhVxbXgMMEAXg=="
                                             }) }).toPromise().then();
    return resultado;
  }

}
