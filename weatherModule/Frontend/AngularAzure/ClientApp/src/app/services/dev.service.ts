import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DevService {

  _debugger:boolean=false;
  _alert:boolean=false;

  constructor(private devSVC:DevService,){
    this._debugger = environment.debugger=="SI";
    this._alert = environment.alert=="SI";
  }

  debugger(){
  if (this._debugger) debugger;
  }

  alert(datos:any){
    if    ( this._alert ) alert (datos);
  }

  log(datos:string, nivel?:string){
    switch (environment.level) {
      case "ERROR":
        if ((environment.level == nivel) || (nivel == "INFO")) {
          console.log(datos);
        }
      case "INFO":
        if ((environment.level==nivel) || (nivel == "ERROR")){
          console.log(datos);
        } 
      case "DEBUG":
        console.log(datos);
        break;
    }
  }

}
