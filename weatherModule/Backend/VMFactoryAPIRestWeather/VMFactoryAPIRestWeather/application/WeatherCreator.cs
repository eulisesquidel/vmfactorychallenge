﻿using System;
using System.Collections.Generic;
using System.Text;
using VMFactoryAPIRestWeather.domain.model;
using VMFactoryAPIRestWeather.model;

namespace VMFactoryAPIRestWeather.application
{
     class WeatherCreator
    {
        IWheatherRepository _weatherRepository;

        public WeatherCreator(IWheatherRepository weather)
        {
            this._weatherRepository = weather;
        }

        public bool @Save(CreateWeather weatherData)
        {
            return this._weatherRepository.CreateWeather(weatherData); ;
        }

    }
}
