﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using VMFactoryAPIRestWeather.domain.model;
using VMFactoryAPIRestWeather.model;

namespace VMFactoryAPIRestWeather.insfraestructure
{
    public class SQLWeatherRepository : IWheatherRepository
    {

        private readonly ILogger log;



        public SQLWeatherRepository(ILogger log)
        {
            this.log = log;
        }

        public bool CreateWeather(CreateWeather weather)
        {

            try
            {
                using (SqlConnection connection = new SqlConnection(Environment.GetEnvironmentVariable("SqlConnectionString")))
                {
                    connection.Open();


                        

                        /* 
                         * var query = $"SET DATEFORMAT DMY; INSERT INTO [WeatherHistory] (" +
                        $"[Country], " +
                        $"[City], " +
                        $"[Temperature], " +
                        $"[FeelsLike], " +
                        $"[CreatedOn], " +
                        $"[UserID] " +
                         $") VALUES('{weather.Country}', '{weather.City}' ," +
                         $" {weather.Temperature.ToString().Replace(",",".")}," +
                         $"{weather.FeelsLike.ToString().Replace(",", ".")}," +
                         $"'{weather.CreatedOn.ToString()}','{weather.UserID}');SELECT CAST(scope_identity() AS int)";
                        */

                        var query = $"SET DATEFORMAT DMY;";
                        log.LogDebug(query);
                        SqlCommand command = new SqlCommand(query, connection);
                        command.ExecuteNonQuery(); // Configuramos el formato de fecha para la session

                        query = $"SET DATEFORMAT DMY; INSERT INTO [WeatherHistory] (" +
                        $"[Country], " +
                        $"[City], " +
                        $"[Temperature], " +
                        $"[FeelsLike], " +
                        $"[CreatedOn], " +
                        $"[UserID] " +
                        $") VALUES(@Country, @City ,@Temperature,@FeelsLike,@CreatedOn,@UserID)";
                        log.LogDebug(query);

                        SqlCommand cmdInsert = new SqlCommand(query, connection);
                        cmdInsert.Parameters.Add("Country", SqlDbType.VarChar, 50).Value = weather.Country;
                        cmdInsert.Parameters.Add("City", SqlDbType.VarChar, 50).Value = weather.City;
                        cmdInsert.Parameters.Add("Temperature", SqlDbType.Decimal, 50).Value = weather.Temperature;
                        cmdInsert.Parameters.Add("FeelsLike", SqlDbType.Decimal, 50).Value = weather.FeelsLike;
                        cmdInsert.Parameters.Add("CreatedOn", SqlDbType.DateTime, 50).Value = weather.CreatedOn;
                        cmdInsert.Parameters.Add("UserID", SqlDbType.VarChar, 50).Value = weather.UserID;

                        cmdInsert.ExecuteScalar();

                         /*
                        query = $"SELECT CAST(scope_identity() AS int);";
                        log.LogDebug(query);
                        SqlCommand cmdgetId = new SqlCommand(query, connection);
                        cmdgetId.ExecuteScalar();
                        */

                }
            }
            catch (Exception e)
            {
                log.LogError(e.ToString());
                return false;
            }

            return true;
        }

        List<WeatherModel> IWheatherRepository.GetWheatherHistory(string UserId)
        {
            throw new NotImplementedException();
        }
    }
}
