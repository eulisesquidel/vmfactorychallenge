using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data.SqlClient;
using VMFactoryAPIRestWeather.model;
using System.Net.Http;
using VMFactoryAPIRestWeather.utils;
using VMFactoryAPIRestWeather.insfraestructure;

namespace VMFactoryAPIRestWeather
{
    public static class WeatherListFunction
    {
        

        [FunctionName("GetWheatherHistory")]
        public static async Task<IActionResult> GetWheatherHistory(
           [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "weatherhistory")] HttpRequest req, ILogger log)
        {

            log.LogInformation("Begin GetWheatherHistory");
            List<WeatherModel> weatherHistoryList = new List<WeatherModel>();
            try
            {
                string cityName = req.Query["cityname"];
                using (SqlConnection connection = new SqlConnection(Environment.GetEnvironmentVariable("SqlConnectionString")))
                {
                    connection.Open();
                    log.LogInformation(cityName.Substring(0, cityName.IndexOf(",") - 1));
                    var query = @"Select top 10 * from WeatherHistory where city  collate SQL_Latin1_General_Cp1_CI_AI like '%" + cityName.Substring(0,cityName.IndexOf(",")-1) + "%' collate SQL_Latin1_General_Cp1_CI_AI Order by CreatedOn desc ";
                    
                    log.LogDebug(query);
                    SqlCommand command = new SqlCommand(query, connection);
                    var reader = await command.ExecuteReaderAsync();
                    while (reader.Read())
                    {
                        WeatherModel oneWeather = new WeatherModel()
                        {
                            Id = (int)reader["ID"],
                            Country = reader["Country"].ToString(),
                            City = reader["City"].ToString(),
                            CreatedOn = (DateTime)reader["CreatedOn"],
                            Temperature = (Decimal)reader["Temperature"],
                            FeelsLike = (Decimal)reader["FeelsLike"],
                            UserID = reader["UserID"].ToString(),
                        };
                        weatherHistoryList.Add(oneWeather);
                    }
                }
            }
            catch (Exception e)
            {
                log.LogError(e.ToString());
            }
            if (weatherHistoryList.Count > 0)
            {
                return new OkObjectResult(weatherHistoryList);
            }
            else
            {
                return new NotFoundResult();
            }

        } // End GetWheatherHistory Method


        private static readonly HttpClient HttpClientWeatherData = new HttpClient();

        [FunctionName("GetCurrentWheather")]
        public static async Task<IActionResult> GetCurrentWheather(
       [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "currentweather")] HttpRequest req, ILogger log)
        {

            log.LogInformation("Begin GetCurrentWheather");

            //plug in your own key, to use locally
            string apiKey = Environment.GetEnvironmentVariable("WeatherApiKey") ;  // dar de alta en azure confiug

            //so use "?cityname=" in the query string
            string cityName = req.Query["cityname"];

            HttpResponseMessage responseMessage = await GetCurrentWeatherData(cityName, apiKey, log);
            if (responseMessage.IsSuccessStatusCode)
            {
                OpenWeatherData resultAPI = (OpenWeatherData)responseMessage.Content.ReadAsAsync(typeof(OpenWeatherData)).Result;
                Console.WriteLine(GradesConvert.KelvinToCelsius(resultAPI.main.temp)); // temperatura
                Console.WriteLine(GradesConvert.KelvinToCelsius(resultAPI.main.feels_like)); // sensacion termica
                Console.WriteLine(resultAPI.coord.lat);
                Console.WriteLine(resultAPI.coord.lon);

                
                SQLWeatherRepository sqlRepo  = new SQLWeatherRepository(log);
                CreateWeather weather = new CreateWeather();
                weather.Country = resultAPI.sys.country;
                weather.City = resultAPI.name;
                weather.Temperature = (Decimal)GradesConvert.KelvinToCelsius(resultAPI.main.temp);
                weather.FeelsLike = (Decimal)GradesConvert.KelvinToCelsius(resultAPI.main.feels_like);
                weather.UserID = "Testing";
                // persist BD
                sqlRepo.CreateWeather(weather);

   

                return new OkObjectResult(weather);

            }

            return new NotFoundResult();
        }

        //grabbing the data off OpenWeather
        private static async Task<HttpResponseMessage> GetCurrentWeatherData(string cityName, string apiKey, ILogger log)
        {
            log.LogInformation("Begin GetCurrentWeatherData");
            return await HttpClientWeatherData.GetAsync($"https://api.openweathermap.org/data/2.5/weather?q={cityName}&appid={apiKey}");
        }


    } // End class  WeatherListFunction


}
