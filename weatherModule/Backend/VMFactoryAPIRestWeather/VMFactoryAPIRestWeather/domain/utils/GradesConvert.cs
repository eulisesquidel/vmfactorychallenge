﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VMFactoryAPIRestWeather.utils
{
    public class GradesConvert
    {

        
         // 0 K − 273.15 = -273.1 °C
        static public double KelvinToCelsius(double temp)
        {
            return Math.Round(temp - 273.15,2);
        }


    }
}
