﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VMFactoryAPIRestWeather.model
{


    public class WeatherModel
    {
        public int Id { get; set; }

        public string City { get; set; }
        public Decimal Temperature { get; set; }

        public Decimal FeelsLike { get; set; }

        public DateTime CreatedOn { get; set; }
        public string Country { get; internal set; }

        public string UserID { get; internal set; }



    }


    public class CreateWeather
    {

        public int Id { get; set; }

        public string City { get; set; }
        public Decimal Temperature { get; set; }

        public Decimal FeelsLike { get; set; }

        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;

        public string Country { get; internal set; }

        public string UserID { get; internal set; }


    }



}
