﻿using System;
using System.Collections.Generic;
using System.Text;
using VMFactoryAPIRestWeather.model;

namespace VMFactoryAPIRestWeather.domain.model
{
    public interface IWheatherRepository
    {
        bool CreateWeather(CreateWeather weather);
        List<WeatherModel> GetWheatherHistory(string UserId);


    }
}
